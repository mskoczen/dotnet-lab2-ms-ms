﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GameOfLife
{
    public partial class Form1 : Form
    {

        int SIZE = 20;
        List<List<Button>> komorki = new List<List<Button>>();
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            for (int i = 0; i < SIZE; i++)
            {
                komorki.Add(new List<Button>());
                for (int j = 0; j < SIZE; j++)
                {
                    Button b = new Button();
                    b.Text = "0";
                    b.Name = "0";
                    b.Height = panel1.Height / SIZE;
                    b.Width = panel1.Width / SIZE;
                    b.Top = i * panel1.Height / SIZE;
                    b.Left = j * panel1.Width / SIZE;
                    b.BackColor = Color.White;

                    b.Click += new EventHandler(ButtonClick);
                    b.TextChanged += new EventHandler(ColorChange);
                    this.panel1.Controls.Add(b);
                    komorki[i].Add(b);

                }
            }

        }

        protected void ButtonClick(object sender, EventArgs e)
        {
            Button button = sender as Button;
            if (button.Text == "0")
                button.Text = "1";
            else
                button.Text = "0";
        }
        protected void ColorChange(object sender, EventArgs e)
        {
            Button button = sender as Button;
            if (button.Text == "1")
                button.BackColor = Color.Green;
            else
                button.BackColor = Color.White;
            
        }

        private void button1_Click(object sender, EventArgs e)
        {
            timer1.Start();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            timer1.Stop();
        }

        private void timer1_Tick(object sender, EventArgs e)

        {

            foreach (List<Button> l in komorki)
            {
                foreach (Button b in l)
                {
                    b.Name = b.Text;
                }
            }


            for(int wiersz=0; wiersz< SIZE; wiersz++)
            {
                for(int kolumna=0; kolumna< SIZE; kolumna++)
                {
                    int sasiad = 0;
                    for(int i=wiersz-1; i<wiersz+2;i++)
                    {
                        for(int j=kolumna-1; j<kolumna+2;j++)
                        {
                            if(!(j==kolumna&&i== wiersz))
                            {
                                if (j > -1 && j < SIZE && i > -1 && i < SIZE)
                                    if (komorki[i][j].Text == "1")
                                        sasiad++;
                            }
                        }
                    }


                    if (sasiad < 2)
                        komorki[wiersz][kolumna].Name = "0";
                    if (sasiad == 3)
                        komorki[wiersz][kolumna].Name = "1";
                    if (sasiad > 3)
                        komorki[wiersz][kolumna].Name = "0";

                }
            }
            foreach(List<Button> l in komorki)
            {
                foreach(Button b in l)
                {
                    b.Text = b.Name;
                }
            }
        }

    }
}
